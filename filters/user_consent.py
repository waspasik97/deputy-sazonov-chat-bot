from aiogram.filters import BaseFilter
from aiogram.types import CallbackQuery, Message

from database.requests import get_user_consent

class IsAccept(BaseFilter):
    """This filter checks of the user consent to the processing of personal data."""
    async def __call__(self, update: CallbackQuery | Message) -> bool:
        consent_status = await get_user_consent(update)
        return consent_status
