from aiogram.types import Message
from aiogram.enums import ChatType
from aiogram.filters import BaseFilter

class IsPrivate(BaseFilter):
    """This filter checks if the chat is private"""
    async def __call__(self, message: Message) -> bool:
        return message.chat.type == ChatType.PRIVATE


class IsNotPrivate(BaseFilter):
    """This filter checks if the chat is not private"""
    async def __call__(self, message: Message) -> bool:
        return message.chat.type != ChatType.PRIVATE
