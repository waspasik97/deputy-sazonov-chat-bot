from environs import Env

env = Env()
env.read_env()

# Telegram
ADMINS = env.list("ADMINS")
BOT_TOKEN = env.str("BOT_TOKEN")
GROUP_ID = env.str("GROUP_ID")
THREAD_SUGGESTION_ID = env.str("THREAD_SUGGESTION_ID")
THREAD_SUPPORT_ID = env.str("THREAD_SUPPORT_ID")

# Database
DB_USERNAME = env.str("DB_USER")
DB_PASSWORD = env.str("DB_PASS")
DB_HOST = env.str("DB_HOST")
DB_PORT = env.str("DB_PORT")
DB_NAME = env.str("DB_NAME")
DATABASE_URL = f"postgresql+asyncpg://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

# Redis
REDIS_HOST=env.str("REDIS_HOST")
REDIS_PORT=env.str("REDIS_PORT")
REDIS_DB=env.str("REDIS_DB")

# Sentry
SENTRY_SDK=env.str("SENTRY_SDK")
