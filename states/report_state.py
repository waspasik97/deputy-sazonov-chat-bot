from aiogram.fsm.state import StatesGroup, State

class Report(StatesGroup):
    """Sets user states when using the chat bot."""
    choose_language = State()
    choose_type_report = State()
    send_complaint = State()
    send_suggestion = State()
    contact_support = State()
    personal_information = State()
