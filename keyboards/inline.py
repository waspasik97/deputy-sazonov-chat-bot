from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from utils.text import TextMessages

async def get_language_keyboard(action: str) -> InlineKeyboardMarkup:
    """Creates two buttons for choosing language."""
    builder = InlineKeyboardBuilder()
    builder.row(
        InlineKeyboardButton(text="Беларуская", callback_data=f"language_{action}_be"),
        InlineKeyboardButton(text="Русский", callback_data=f"language_{action}_ru")
    )
    builder.adjust(2)
    return builder.as_markup()


async def get_report_keyboard(language: str) -> InlineKeyboardMarkup:
    """Creates a buttons for selecting the type of report."""
    complaint_button, suggestion_button = await TextMessages().start_keyboard_text_button(language)
    builder = InlineKeyboardBuilder()
    builder.row(
        InlineKeyboardButton(text=complaint_button, callback_data="complaint"),
        InlineKeyboardButton(text=suggestion_button, callback_data="suggestion")
    )
    builder.adjust(2)
    return builder.as_markup()


async def get_personal_information_keyboard(language: str) -> InlineKeyboardMarkup:
    """Creates a buttons for accepting processing personal information."""
    accept_button, reject_button = await TextMessages().personal_info_keyboard_text_button(language)
    builder = InlineKeyboardBuilder()
    builder.row(
        InlineKeyboardButton(text=accept_button, callback_data="accept"),
        InlineKeyboardButton(text=reject_button, callback_data="reject")
    )
    builder.adjust(2)
    return builder.as_markup()
