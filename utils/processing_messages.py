from asyncio import sleep

from aiogram.fsm.context import FSMContext
from aiogram.types import Message, InputMediaPhoto, InputMediaVideo, InputMediaDocument

from data.config import GROUP_ID, THREAD_SUGGESTION_ID, THREAD_SUPPORT_ID
from database.requests import set_complaint, set_suggestion
from handlers.start import start
from keyboards.inline import get_report_keyboard
from states.report_state import Report
from utils.message_for_user_and_group import messages_for_user_and_group
from loader import bot

MEDIA_GROUPS = dict()

async def send_messages(
        message_type: str,
        message: Message,
        state: FSMContext,
        message_for_group: str,
        message_for_user: str,
        language: str,
        album=None,
        file=None) -> None:
    """Send a user bug report or suggestion to the developer group."""
    if album:
        if message_type == "complaint":
            await bot.send_media_group(chat_id=GROUP_ID, media=album)
            await bot.send_message(chat_id=GROUP_ID, text=message_for_group)
        elif message_type == "suggestion":
            await bot.send_media_group(chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, media=album)
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, text=message_for_group)
        else:
            await bot.send_media_group(chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, media=album)
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, text=message_for_group)
    elif file:
        if file[0] == "photo":
            if message_type == "complaint":
                await bot.send_photo(chat_id=GROUP_ID, photo=file[-1], caption=message_for_group)
            elif message_type == "suggestion":
                await bot.send_photo(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, photo=file[-1], caption=message_for_group
                )
            else:
                await bot.send_photo(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, photo=file[-1], caption=message_for_group
                )
        elif file[0] == "video":
            if message_type == "complaint":
                await bot.send_video(chat_id=GROUP_ID, video=file[-1], caption=message_for_group)
            elif message_type == "suggestion":
                await bot.send_video(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, video=file[-1], caption=message_for_group
                )
            else:
                await bot.send_video(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, video=file[-1], caption=message_for_group
                )
        else:
            if message_type == "complaint":
                await bot.send_document(chat_id=GROUP_ID, document=file[-1], caption=message_for_group)
            elif message_type == "suggestion":
                await bot.send_document(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, document=file[-1], caption=message_for_group
                )
            else:
                await bot.send_document(
                    chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, document=file[-1], caption=message_for_group
                )
    elif message_for_group:
        if message_type == "complaint":
            await bot.send_message(chat_id=GROUP_ID, text=message_for_group)
        elif message_type == "suggestion":    
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_SUGGESTION_ID, text=message_for_group)
        else:    
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_SUPPORT_ID, text=message_for_group)
    markup = await get_report_keyboard(language)
    await message.answer(text=message_for_user, reply_markup=markup)
    await state.set_state(Report.choose_type_report)

    # Set complaint or suggestion data to the database
    if message_type == "complaint":
        await set_complaint(message)
    elif message_type == "suggestion":
        await set_suggestion(message)


async def processing_message(message: Message, state: FSMContext, language: str, message_type: str) -> None:
    """Process a user message containing any files or text."""
    # Processing only text
    if message.text:
        if message.text.lower() == "/start":
            await start(message, state)
        else:
            msg_for_group, msg_for_user = await messages_for_user_and_group(message_type, message, message.text, language)
            await send_messages(message_type, message, state, msg_for_group, msg_for_user, language)
    # Processing only media group
    elif message.media_group_id:
        if message.photo:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(f"{message.photo[-1].file_id}photo")
                return
            MEDIA_GROUPS[message.media_group_id] = [f"{message.photo[-1].file_id}photo"]
        if message.video:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(f"{message.video.file_id}video")
                return
            MEDIA_GROUPS[message.media_group_id] = [f"{message.video.file_id}video"]
        if message.document:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(message.document.file_id)
                return
            MEDIA_GROUPS[message.media_group_id] = [message.document.file_id]
        await sleep(1)
        album = [
            InputMediaPhoto(media=file_id[:-5]) if file_id[-5:] == "photo" else
            InputMediaVideo(media=file_id[:-5]) if file_id[-5:] == "video" else
            InputMediaDocument(media=file_id)
            for file_id in MEDIA_GROUPS[message.media_group_id]
        ]
        MEDIA_GROUPS.pop(message.media_group_id, None)
        msg_for_group, msg_for_user = await messages_for_user_and_group(message_type, message, message.caption, language)
        await send_messages(message_type, message, state, msg_for_group, msg_for_user, language, album)
    # Processing only single media file
    elif message.photo or message.video or message.document:
        if message.photo:
            file_id = message.photo[-1].file_id
            file_type = "photo"
        elif message.video:
            file_id = message.video.file_id
            file_type = "video"
        else:
            file_id = message.document.file_id
            file_type = "document"
        file_info = (file_type, file_id)
        msg_for_group, msg_for_user = await messages_for_user_and_group(message_type, message, message.caption, language)
        await send_messages(message_type, message, state, msg_for_group, msg_for_user, language, album=None, file=file_info)
        