from aiogram.types import Message

from utils.text import TextMessages

async def messages_for_user_and_group(msg_type: str, message: Message, text: str, language: str) -> tuple[str]:
    """Get messages for the user and developer group."""
    message_for_group = await TextMessages().message_for_developer_group(
        user_name=message.from_user.username,
        full_name=message.from_user.full_name,
        text=text
    )
    message_for_user = await TextMessages().suppor_thankful_messaget(language)
    if msg_type != "support":
        message_for_user = await TextMessages().thankful_message(language)
    return message_for_group, message_for_user
