import logging

async def send_error_to_user(error: Exception) -> None:
    logging.exception("Error while getting data to API: " + str(error))
