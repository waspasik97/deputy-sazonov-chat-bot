from aiogram.types import BotCommand, BotCommandScopeDefault, BotCommandScopeChat

# from data.config import GROUP_ID
from utils.text import TextMessages
from loader import bot

COMMAND_NAMES = [
    "start",
    "language",
    "report",
    "support",
    "personalinfo",
    "instruction"
]

async def set_bot_commands(descriptions=None) -> None:
    """Set default commands in bot menu for chat bot."""
    if not descriptions:
        descriptions = await TextMessages().commands_description("ru")
    commands = [
        BotCommand(command=COMMAND_NAMES[i], description=descriptions[i])
        for i in range(len(COMMAND_NAMES))
    ]
    await bot.set_my_commands(commands=commands, scope=BotCommandScopeDefault())


# async def set_chat_commands() -> None:
#     """Set default commands in bot menu for chat bot."""
#     descriptions = ["Заблокировать",]
#     command_names = ["block",]
#     commands = [
#         BotCommand(command=command_names[i], description=descriptions[i])
#         for i in range(len(command_names))
#     ]
#     await bot.set_my_commands(commands=commands, scope=BotCommandScopeChat(chat_id=GROUP_ID))


async def set_default_commands():
    await set_bot_commands()
    # await set_chat_commands()
