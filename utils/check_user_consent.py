from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery

from filters.user_consent import IsAccept
from keyboards.inline import get_personal_information_keyboard
from states.report_state import Report
from utils.text import TextMessages

async def check_consent_status(callback_query: CallbackQuery, language: str, state: FSMContext) -> bool:
    """
    Check user consent status.
    Return True if the user has consented to the processing of personal data else False.
    """
    if await IsAccept().__call__(callback_query):
        return True
    text = await TextMessages().reject_personal_info(language)
    markup = await get_personal_information_keyboard(language)
    await callback_query.message.edit_text(text=text, reply_markup=markup)
    await state.set_state(Report.personal_information)
    return False
