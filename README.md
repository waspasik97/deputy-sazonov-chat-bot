# Deputy Sazonov Chat Bot


## About project (EN)

Feedback bot with deputy Sazonov. The bot allows voters of the district to send their complaints, comments and suggestions to the deputy in a specially created chat in Telegram.

The bot supports the following features:
- Sending messages containing media files to the deputy’s chat;
- Obtaining consent to the processing of users’ personal data;
- Recording information about applicants into the database, including storing and changing the status of consent to the processing of personal data;
- Contacting chat bot technical support;
- Localization of the chat bot in two state languages ​​of the Republic of Belarus (Belarusian and Russian).

## О проекте (RU)

Бот обратной связи с депутатом Сазоновым. Бот позволяет избирателям округа отправить свои жалобы, замечания и предложения депутату в специально созданный чат в Телеграм.

Бот поддерживает следующие функции:
- Отправка в чат депутата сообщений, содержащих медиа файлы;
- Получение согласия на обработку персональных данных пользователей;
- Запись в базу данных сведений о заявителях, в том числе хранение и изменение статуса согласия на обработку персональных данных;
- Обращение в техническую поддержку чат бота;
- Локализация чат бота на двух государственных языках Республики Беларусь (белорусском и русском).


## Installation

### Install for development

Prerequisites: SQLAlchemy as database, Redis as storage, Python 3.11 and pip.

1. Clone this repo `git clone https://gitlab.com/waspasik97/deputy-sazonov-chat-bot.git` and go to it's root directory.
2. Install dependencies: `pip install -r requirements.txt`.
3. Rename `.env.template` file in the root folder to `.env` and fill it in. Use value "localhost" for the DB_HOST and REDIS_HOST variables.
4. If you want to use the bot locally on your machine using an SQLAlchemy database. In file databasa/models.py for the function 'create_async_engine' (9 line) use the value for the 'url' parameter equal `sqlite+aiosqlite:///db.sqlite3`.
5. Run the bot `python bot.py`.

### Run in Docker

1. Clone this repo `git clone https://gitlab.com/waspasik97/deputy-sazonov-chat-bot.git` and go to it's root folder.
2. Rename `.env.template` file in the root folder to `.env` and fill it in. Values ​​for the DB_HOST and REDIS_HOST variables specify equal values ​​for the host names in the postgres_main and redis containers (DB_HOST=postgres_main, REDIS_HOST=redis).
3. Run `docker build . -t deputy-sazonov`.
4. Run `docker compose up --build`.
5. Pgadmin is available at http://localhost:5050.
