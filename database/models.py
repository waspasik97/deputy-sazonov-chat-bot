from sqlalchemy import BigInteger, String, ForeignKey, Text, Boolean
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from data.config import DATABASE_URL

# If you want to use the bot locally on your machine using an sqlalchemy database,
# use the value for the URL parameter "sqlite+aiosqlite:///db.sqlite3"
engine = create_async_engine(url=DATABASE_URL)
async_session = async_sessionmaker(engine)

class Base(DeclarativeBase):
    pass


class User(Base):
    """Stores user info."""
    __tablename__ = "users"

    tg_id = mapped_column(BigInteger, primary_key=True)
    first_name: Mapped[str] = mapped_column(String(64))
    last_name: Mapped[str] = mapped_column(String(64), nullable=True)
    username: Mapped[str] = mapped_column(String(50), nullable=True)
    consent: Mapped[bool] = mapped_column(Boolean)


class Complaint(Base):
    """Stores complaint info."""
    __tablename__ = "complaints"
    
    id: Mapped[int] = mapped_column(primary_key=True)
    user: Mapped[int] = mapped_column(ForeignKey("users.tg_id"))
    full_name: Mapped[str] = mapped_column(String)
    complaint: Mapped[str] = mapped_column(Text, nullable=True)


class Suggestion(Base):
    """Stores suggestion info."""
    __tablename__ = "suggestions"
    
    id: Mapped[int] = mapped_column(primary_key=True)
    user: Mapped[int] = mapped_column(ForeignKey("users.tg_id"))
    full_name: Mapped[str] = mapped_column(String)
    suggestion: Mapped[str] = mapped_column(Text, nullable=True)


async def async_main():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)