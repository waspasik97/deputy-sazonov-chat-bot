from sqlalchemy import select

from aiogram.types import CallbackQuery, Message

from database.models import async_session
from database.models import User, Complaint, Suggestion

async def set_user(update: CallbackQuery | Message, consent_status: str = None) -> None:
    """Set user data to the database."""
    async with async_session() as session:
        consent = {
            "accept": True,
            "reject": False
        }
        user = await session.scalar(select(User).where(User.tg_id==update.from_user.id))

        if not user:
            session.add(
                User(
                    tg_id=update.from_user.id,
                    first_name=update.from_user.first_name,
                    last_name=update.from_user.last_name,
                    username=update.from_user.username,
                    consent=consent[consent_status]
                )
            )
            await session.commit()
        else:
            if consent_status:
                user.consent = consent[consent_status]
                await session.commit()


async def get_user_consent(update: Message) -> bool:
    """Return user consent to the processing of personal data."""
    async with async_session() as session:
        user = await session.scalar(select(User).where(User.tg_id==update.from_user.id))
        return user.consent


async def set_complaint(update: Message) -> None:
    """Set user complaint to the database."""
    async with async_session() as session:
        complaint = update.text
        if update.caption:
            complaint = update.caption
        
        session.add(
            Complaint(
                user=update.from_user.id,
                full_name=update.from_user.full_name,
                complaint=complaint
            )
        )
        await session.commit()


async def set_suggestion(update: Message) -> None:
    """Set user suggestion to the database."""
    async with async_session() as session:
        suggestion = update.text
        if update.caption:
            suggestion = update.caption
        
        session.add(
            Suggestion(
                user=update.from_user.id,
                full_name=update.from_user.full_name,
                suggestion=suggestion
            )
        )
        await session.commit()
