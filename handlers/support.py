from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from filters.chat_type import IsPrivate
from filters.user_consent import IsAccept
from handlers.language import get_language
from keyboards.inline import get_personal_information_keyboard
from middlewares.throttling import ThrottlingMiddleware
from states.report_state import Report
from utils.processing_messages import processing_message 
from utils.text import TextMessages
from loader import redis

support_router = Router()
support_router.message.middleware(ThrottlingMiddleware(redis))
contact_support_router = Router()

@contact_support_router.message(Report.contact_support, F.photo | F.video | F.document | F.text)
async def processing_message_about_suggestion(message: Message, state: FSMContext) -> None:
    """Begin the process of processing a user message about suggestion."""
    state_data = await state.get_data()
    language = state_data["language"]
    await processing_message(message, state, message_type="support", language=language)


@support_router.message(IsPrivate(), Command("support"))
async def support(message: Message, state=FSMContext) -> None:
    """Sends a message about support to chat."""
    language = await get_language(user_key=f"language:{message.from_user.id}")

    # Check user consent status
    if not await IsAccept().__call__(message):
        text = await TextMessages().reject_personal_info(language)
        markup = await get_personal_information_keyboard(language)
        await message.answer(text=text, reply_markup=markup)
        await state.set_state(Report.personal_information)
        return
    
    text = await TextMessages().about_support(language)
    await message.answer(text=text)
    await state.set_state(Report.contact_support)
    await state.set_data({"language": language})
