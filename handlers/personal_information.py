from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message
from aiogram.types.input_file import FSInputFile


from database.requests import set_user
from filters.chat_type import IsPrivate
from handlers.language import get_language
from keyboards.inline import get_report_keyboard, get_personal_information_keyboard
from middlewares.throttling import ThrottlingMiddleware
from states.report_state import Report
from utils.text import TextMessages
from loader import redis

personal_information_router = Router()
personal_information_router.message.middleware(ThrottlingMiddleware(redis))

@personal_information_router.message(IsPrivate(), Command("personalinfo"))
async def personal_info(message: Message, state: FSMContext) -> None:
    """Send message about choose language and keyboard with actions."""
    language = await get_language(user_key=f"language:{message.from_user.id}")
    document = FSInputFile("data/Processing_personal_data.docx")
    personal_info_message = await TextMessages().about_personal_info(language)
    markup = await get_personal_information_keyboard(language)
    await message.answer_document(document=document, caption=personal_info_message, reply_markup=markup)
    await state.set_state(Report.personal_information)


@personal_information_router.message(IsPrivate(), Command("report"))
async def report(message: Message, state: FSMContext) -> None:
    """Send message about sending a complaint or suggestion."""
    language = await get_language(user_key=f"language:{message.from_user.id}")
    report_message = await TextMessages().send_report_message(language)
    markup = await get_report_keyboard(language)
    await message.answer(text=report_message, reply_markup=markup)
    await state.set_state(Report.choose_type_report)

    # Set or update user data to the database
    await set_user(message)


@personal_information_router.callback_query(Report.personal_information, F.data=="accept")
async def choosing_type_report(callback_query: CallbackQuery, state: FSMContext) -> None:
    """
    Send message about initialization and message with buttons sending a complaint
    or suggestion.
    """
    language = await get_language(user_key=f"language:{callback_query.from_user.id}")

    # Send welcom message after initialization
    welcom_message = await TextMessages().welcome_message(
        language, callback_query.from_user.full_name
    )
    await callback_query.message.answer(text=welcom_message)
    await callback_query.message.answer(text=".\n.\n.")

    # Send buttons and message about sending a complaint or suggestion
    report_message = await TextMessages().send_report_message(language)
    markup = await get_report_keyboard(language)
    await callback_query.message.answer(text=report_message, reply_markup=markup)
    await callback_query.message.delete()
    
    await state.set_state(Report.choose_type_report)
    
    # Set or update user data to the database
    await set_user(callback_query, consent_status="accept")


@personal_information_router.callback_query(Report.personal_information, F.data=="reject")
async def reject_giving_consent(callback_query: CallbackQuery, state: FSMContext) -> None:
    """
    Send message and buttons sending a complaint or suggestion if user reject giving
    consent to the processing of personal data.
    """
    language = await get_language(f"language:{callback_query.from_user.id}")
    text_message = await TextMessages().reject_personal_info(language)
    markup = await get_personal_information_keyboard(language)
    await callback_query.message.edit_caption(caption=text_message, reply_markup=markup)
    await state.set_state(Report.personal_information)

    # Set or update user data to the database
    await set_user(callback_query, consent_status="reject")
