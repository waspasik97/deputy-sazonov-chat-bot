from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

from handlers.language import get_language
from states.report_state import Report
from utils.check_user_consent import check_consent_status
from utils.processing_messages import processing_message
from utils.text import TextMessages

complaint_router = Router()

@complaint_router.message(Report.send_complaint, F.photo | F.video | F.document | F.text)
async def processing_message_about_problem(message: Message, state: FSMContext) -> None:
    """Begin the process of processing a user message about found bug."""
    state_data = await state.get_data() 
    language = state_data["language"]
    await processing_message(message, state, language, message_type="complaint")


@complaint_router.callback_query(Report.choose_type_report, F.data=="complaint")
async def describe_problem(callback_query: CallbackQuery, state: FSMContext) -> None:
    """Send a prompt to the user to describe the found bug."""
    language = await get_language(user_key=f"language:{callback_query.from_user.id}")

    # Check user consent status
    if await check_consent_status(callback_query, language, state):
        description_problem = await TextMessages().about_describing_problem(language)
        await callback_query.message.edit_text(text=description_problem)
        await state.set_state(Report.send_complaint)
        await state.set_data({"language": language})
    return
