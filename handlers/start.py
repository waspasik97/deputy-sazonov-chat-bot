from aiogram import Router
from aiogram.filters import CommandStart
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from keyboards.inline import get_language_keyboard
from middlewares.throttling import ThrottlingMiddleware
from states.report_state import Report
from utils.text import TextMessages
from loader import redis

start_router = Router()
start_router.message.middleware(ThrottlingMiddleware(redis))

@start_router.message(CommandStart())
async def start(message: Message, state=FSMContext) -> None:
    """
    Send message about choose language and keyboard with actions when the user starts
    chatbot.
    """
    language_message = await TextMessages().about_language()
    markup = await get_language_keyboard("noupdate")
    await message.answer(text=language_message, reply_markup=markup)
    await state.set_state(Report.choose_language)
