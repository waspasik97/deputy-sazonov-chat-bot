from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message
from aiogram.types.input_file import FSInputFile

from filters.chat_type import IsPrivate
from keyboards.inline import get_language_keyboard, get_personal_information_keyboard
from middlewares.throttling import ThrottlingMiddleware
from states.report_state import Report
from utils.setting_commands import set_bot_commands
from utils.text import TextMessages
from loader import redis

language_router = Router()
language_router.message.middleware(ThrottlingMiddleware(redis))

async def get_language(user_key: str) -> str:
    """Returns language wich choosed user."""
    language = await redis.hget(name=user_key, key="language")
    return language


@language_router.message(IsPrivate(), Command("language"))
async def language(message: Message) -> None:
    """Send message about choose language and keyboard with actions."""
    language_message = await TextMessages().about_language()
    markup = await get_language_keyboard("update")
    await message.answer(text=language_message, reply_markup=markup)


@language_router.callback_query(F.data.startswith("language"))
async def choose_language(callback_query: CallbackQuery, state=FSMContext) -> None:
    """Set up language for user to the bot chat and update language for user to the adminpanel."""
    user_key = f"language:{callback_query.from_user.id}"
    language = callback_query.data.split("_")[-1]
    await redis.hset(name=user_key, key="language", value=language)
    
    choosed_language_message = await TextMessages().about_chosen_language(language)
    await callback_query.answer(text=choosed_language_message)
    
    # Reset default commands in bot menu for chat bot
    await set_bot_commands(
        descriptions=await TextMessages().commands_description(language)
    )

    # Send welcome message or ignore
    if callback_query.data.split("_")[1] == "update":
        return
    else:
        choosed_language = await get_language(user_key)
        document = FSInputFile("data/Processing_personal_data.docx")
        text_message = await TextMessages().about_personal_info(choosed_language)
        markup = await get_personal_information_keyboard(language)
        await callback_query.message.answer_document(document=document, caption=text_message, reply_markup=markup)
        await callback_query.message.delete()
        await state.set_state(Report.personal_information)
