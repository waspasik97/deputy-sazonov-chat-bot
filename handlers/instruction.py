from aiogram import Router
from aiogram.enums import ParseMode
from aiogram.filters import Command
from aiogram.types import Message

from filters.chat_type import IsPrivate
from handlers.language import get_language
from middlewares.throttling import ThrottlingMiddleware
from utils.text import TextMessages
from loader import redis

instruction_router = Router()
instruction_router.message.middleware(ThrottlingMiddleware(redis))

@instruction_router.message(IsPrivate(), Command("instruction"))
async def instruction(message: Message) -> None:
    """Sends a instruction text to the chat with bot (private chat)."""
    language = await get_language(user_key=f"language:{message.from_user.id}")
    text = await TextMessages().about_instruction(language)
    await message.answer(text=text, parse_mode=ParseMode.HTML, disable_web_page_preview=True)
