from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

from handlers.language import get_language
from states.report_state import Report
from utils.check_user_consent import check_consent_status
from utils.processing_messages import processing_message 
from utils.text import TextMessages

suggestion_router = Router()

@suggestion_router.message(Report.send_suggestion, F.photo | F.video | F.document | F.text)
async def processing_message_about_suggestion(message: Message, state: FSMContext) -> None:
    """Begin the process of processing a user message about suggestion."""
    state_data = await state.get_data()
    language = state_data["language"]
    await processing_message(message, state, language, message_type="suggestion")


@suggestion_router.callback_query(Report.choose_type_report, F.data=="suggestion")
async def describe_suggestion(callback_query: CallbackQuery, state: FSMContext) -> None:
    """Send a prompt to the user to describe the suggestion."""
    language = await get_language(user_key=f"language:{callback_query.from_user.id}")

    # Check user consent status
    if await check_consent_status(callback_query, language, state):
        description_suggestion = await TextMessages().about_describing_suggestion(language)
        await callback_query.message.edit_text(text=description_suggestion)
        await state.set_state(Report.send_suggestion)
        await state.set_data({"language": language})
    return
