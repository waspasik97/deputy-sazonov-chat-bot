"""Import all routers and add them to routers_list."""
from .start import start_router
from .complaint import complaint_router
from .suggestion import suggestion_router
from .instruction import instruction_router
from .support import support_router, contact_support_router
from .personal_information import personal_information_router
from .language import language_router

routers_list = [
    start_router,
    complaint_router,
    suggestion_router,
    instruction_router,
    support_router,
    contact_support_router,
    personal_information_router,
    language_router,
]

__all__ = [
    "routers_list",
]