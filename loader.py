from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.fsm.storage.redis import Redis
from aiogram.fsm.storage.memory import MemoryStorage

from data.config import BOT_TOKEN, REDIS_HOST, REDIS_PORT, REDIS_DB

redis: Redis = Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, decode_responses=True)

bot: Bot = Bot(token=BOT_TOKEN, parse_mode=ParseMode.HTML)
storage = MemoryStorage()
dp: Dispatcher = Dispatcher(storage=storage)
